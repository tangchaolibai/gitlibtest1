package com.forms;

import java.io.File;
import java.io.FileInputStream;
import java.io.RandomAccessFile;
import java.util.Properties;

import javax.management.RuntimeErrorException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ModifySqlConstraintNameBatch {
	private static final Logger logger = LoggerFactory.getLogger(ModifySqlConstraintNameBatch.class);
	
	private static String metaFilePath;
	private static String scriptsAction;
	
	private static final String SQL_SUFFIX = ".sql";
	
	private static final String SCRIPTS_ACTION_CREATE = "create";
	private static final String SCRIPTS_ACTION_DROP = "drop";
	private static final String SCRIPTS_ACTION_DELETE = "delete";
	
	private static String tableName = "";
	
	static {
		try {
			loadProperties();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}
	
	public static void main(String[] args) {
		try {
			modifySql(metaFilePath, scriptsAction);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}
	
	private static void modifySql(String metaFilePath, String scriptsAction) {
		if("".equals(metaFilePath) || "".equals(scriptsAction)) {
			throw new RuntimeErrorException(null, "Wrong:config params can not be empty!");
		}
		
		File file = new File(metaFilePath);
		if(!file.exists()) {
			throw new RuntimeErrorException(null, "Wrong:metaFile is non-exist!");
		}
		
		if(!(metaFilePath.length() - SQL_SUFFIX.length() == metaFilePath.indexOf(SQL_SUFFIX))) {
			throw new RuntimeErrorException(null, "Wrong:meta file is not a '.sql' file!");
		}
		
		String newFilePath = metaFilePath.split("\\.sql")[0] + "-" + scriptsAction + SQL_SUFFIX;
		File fileNew = new File(newFilePath);
		if (fileNew.exists()) {
			fileNew.delete();
			System.gc();
		}
		
		String line = "";
		int num = 1;
		try (RandomAccessFile raf = new RandomAccessFile(file, "r"); RandomAccessFile rafNew = new RandomAccessFile(fileNew, "rw")) {
			while ((line = raf.readLine()) != null) {
				if(SCRIPTS_ACTION_CREATE.equals(scriptsAction)) {
					if(line.startsWith("alter table") && !line.contains("foreign key") && line.contains("unique")) {
						// UK: alter table unique
						if ("".equals(tableName)) tableName = getTableName(line);
						
						if(!line.contains(tableName)) num = 1;
						
						line = modifyUK(line, num);
						num++; 
					}
					
					if(line.startsWith("alter table") && line.contains("foreign key") && !line.contains("unique")) {
						// FK: create
						if ("".equals(tableName)) tableName = getTableName(line);
						
						if(!line.contains(tableName)) num = 1;
						
						line = modifyFK(line, num);
						num++; 
					}
				}else if (SCRIPTS_ACTION_DROP.equals(scriptsAction)) {
					if(line.startsWith("alter table") && line.contains("drop foreign key") && !line.contains("FK_")) {
						// FK: drop
						if ("".equals(tableName)) tableName = getTableName(line);
						
						if(!line.contains(tableName)) num = 1;
						
						line = dropFK(line, num);
						num++; 
					}
				}else if (SCRIPTS_ACTION_DELETE.equals(scriptsAction)) {
					if(line.startsWith("alter table") && line.contains("drop foreign key") && !line.contains("FK_")) {
						// FK: drop
						if ("".equals(tableName)) tableName = getTableName(line);
						
						if(!line.contains(tableName)) num = 1;
						
						line = dropFK(line, num);
						num++; 
					}
					
					if(line.startsWith("drop table")) {
						// Generate Delete
						tableName = getTableName(line);
						
						line = generateDel(line);
					}
				}else {
					throw new RuntimeErrorException(null, "Wrong:unknown config param 'scripts-action'!");
				}
				
				line = line + "\r\n";
				rafNew.writeBytes(line);
            }
        } catch (Exception e) {
        	fileNew.delete();
        	System.gc();
        	throw new RuntimeErrorException(null, "Wrong:please fill in correct config params!");
        }
		logger.info("Completed!");
	}
	
	private static String modifyFK(String line, int num) {
		logger.info("Ori sql:=========={}", line);
		
		String FKTableName = line.split("add constraint")[0].split("alter table")[1].trim();
		tableName = FKTableName + " add constraint";
		String FK_part2 = line.split("foreign key")[1];
		String FKsql = "FK_" + FKTableName + "_" + num + " foreign key" + FK_part2;
		line = line.replaceAll("FK.*", FKsql);
		
		logger.info("Generate sql:=========={}", line);
		return line;
	}
	
	private static String getTableName(String line) {
		String result = "";
		if(SCRIPTS_ACTION_CREATE.equals(scriptsAction)) {
			result = line.split("add constraint")[0].split("alter table")[1].trim() + " add constraint";
		}else if (SCRIPTS_ACTION_DROP.equals(scriptsAction)) {
			result = line.split("drop foreign key")[0].split("alter table")[1].trim() + " drop foreign key";
		}else if (SCRIPTS_ACTION_DELETE.equals(scriptsAction)) {
			String[] tempArr = line.split(" ");
			result = tempArr[tempArr.length - 1].trim().split(";")[0].trim();
		}
		
		return result;
	}
	
	private static String dropFK(String line, int num) {
		logger.info("Ori sql:=========={}", line);
		
		String FKTableName = line.split("drop foreign key")[0].split("alter table")[1].trim();
		tableName = FKTableName + " drop foreign key";
		String FKsql = "FK_" + FKTableName + "_" + num +";";
		line = line.replaceAll("FK.*", FKsql);
		
		logger.info("Generate sql:=========={}", line);
		return line;
	}
	
	private static String modifyUK(String line, int num) {
		logger.info("Ori sql:=========={}", line);
		
		String UKTableName = line.split("add constraint")[0].split("alter table")[1].trim();
		tableName = UKTableName + " add constraint";
		String[] UK_part2_arr = line.split("add constraint")[1].trim().split(" ");
		String UK_part2 = ""; 
		UK_part2 = (UK_part2_arr.length == 2) ? UK_part2_arr[1] : UK_part2_arr[1] + " " + UK_part2_arr[2];
		
		String UKsql = "UK_" + UKTableName + "_" + num + " " + UK_part2;
		line = line.replaceAll("UK.*", UKsql);
		
		logger.info("Generate sql:=========={}", line);
		return line;
	}
	
	private static String generateDel(String line) {
		logger.info("Ori sql:=========={}", line);
		
		line = "truncate table " + tableName + ";";
		
		logger.info("Generate sql:=========={}", line);
		return line;
	}
	
	private static void loadProperties() {
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream(new File("config.properties")));
			metaFilePath = properties.getProperty("metaFilePath");
			scriptsAction = properties.getProperty("scripts.action");
		} catch (Exception e) {
			throw new RuntimeErrorException(null, "Wrong:can not get config params, please check out the config!");
		} 
	}
	
}
